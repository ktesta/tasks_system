<?php

require "PSQL_OTRS.php";
require 'PHPMailerAutoload.php';

$tn = $argv[1];
try {
	$sql = "SELECT 
	            ticket.id ticket_id, 
	            tn, 
	            ticket.title, 
	            ticket_type.name ticket_type, 
	            ticket_state.name status, 
	            to_char(ticket.create_time, 'DD/MM/YYYY HH24:MI' ) ticket_create, 
	            ticket.change_time ticket_change, 
	            customer_user.first_name as first_name, 
	            customer_user.last_name last_name, 
	            customer_user.email 
	        FROM ticket 
	        LEFT JOIN ticket_type ON (ticket.type_id = ticket_type.id) 
	        LEFT JOIN ticket_state ON (ticket.ticket_state_id = ticket_state.id) 
	        LEFT JOIN customer_user ON (ticket.customer_user_id = customer_user.login)
	        WHERE 
	            ticket.tn = '".$tn."' ";

	$ticket_attr = pg_fetch_array(pg_query($sql));

	if (empty($ticket_attr['ticket_id'])) {
		throw new Exception('Ticket não existe.');
	}

	$sql = "SELECT a_body log FROM article 
	        WHERE 
	            ticket_id = '".$ticket_attr['ticket_id']."' 
	        ORDER BY create_time ASC 
	        LIMIT 1";

	$log_attr = pg_fetch_array(pg_query($sql));
	$log = str_replace("\n", "<br/>", $log_attr['log']);

	$sql = "SELECT a_body log FROM article 
	        WHERE 
	            ticket_id = '".$ticket_attr['ticket_id']."' 
	        ORDER BY create_time DESC 
	        LIMIT 1";

	$log_attr = pg_fetch_array(pg_query($sql));
	$last_log = str_replace("\n", "<br/>", $log_attr['log']);

	$sql = "SELECT value_text, field_id FROM dynamic_field_value 
	        WHERE 
	            object_id = '".$ticket_attr['ticket_id']."' AND 
	            field_id IN (3,4)";

	$query = pg_query($sql);
	$fields = array(3 => "", 4 => "");
	while ($f = pg_fetch_array($query)) {
		$fields[$f['field_id']] = ucfirst($f['value_text']);
	}

	$html = "<html>
		        <head>
		        	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		            <title>Chamado finalizado</title>
		        </head>
		        <body>
		           Olá, ".$ticket_attr['first_name']. " " . $ticket_attr['last_name'] .". 
		           <p>O ticket $tn foi encerrado.
		           <br/><br/>
		           <table border=1 class='table' >
		           		<tr>
		           			<th>Ticket:</th>
		           			<td>$tn</td>
		           		</tr>
		           		<tr>
		           			<th>Título:</th>
		           			<td>".$ticket_attr['title']."</td>
		           		</tr>
		           		<tr>
		           			<th>Tipo:</th>
		           			<td>".$ticket_attr['ticket_type']."</td>
		           		</tr>
		           		<tr>
		           			<th>Outros:</th>
		           			<td>$log</td>
		           		</tr>
		           		<tr>
		           			<th>Abertura:</th>
		           			<td>".$ticket_attr['ticket_create']."</td>
		           		</tr>
		           		<tr>
		           			<th>Causa:</th>
		           			<td>".$fields[3]."</td>
		           		</tr>
		           		<tr>
		           			<th>Solução:</th>
		           			<td>".$fields[4]."</td>
		           		</tr>	           		
		           </table>
		           <br>
		           <table border=1 class='table' >
		           		<tr>
		           			<th>Conclusão</th>
		           			<td>".$last_log."</td>	           		
		           		</tr>	           		
		           </table>
		           <p> Para mais detalhes <a href='https://helpdesk.horizonstelecom.com/helpdesk/history'>clique aqui</a>.
		           <br><br><br>
		           <hr>
		           <img  height='60' src='https://helpdesk.horizonstelecom.com/oss/helpdesk/public/img/horizons.png'>
		           <p style='font-size: 12' ><a href='www.horizonstelecom.com'>www.horizonstelecom.com</p>
		        </body>
		    </html>";

	$style = '<style> 

	body {
		font-size: 14px;
		font-family: Verdana;
	}

	table {
	  max-width: 100%;
	  background-color: transparent;
	}

	table {
	  border-collapse: collapse;
	  border: 1px solid black ;
	}
	tr { 
	  border: 1px solid black ;
	  border-width: 1px 0;

	}
	th {
		padding: 10px;
	  text-align: left;
	  background-color: #CCC;
	  color: #000;
	}

	td {
		padding: 10px;
	  text-align: left;
	  background-color: #FFF;
	  color: #000;
	}


	</style>';

	$body = $style . " " . $html;

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = "10.100.0.12";
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = 25;
	$mail->CharSet = 'UTF-8';
	//Set who the message is to be sent from
	$mail->setFrom('sistemas.oss@horizonstelecom.com', 'Horizons Help Desk');
	//Set who the message is to be sent to	
	$mail->addAddress($ticket_attr['email']);
	//Set the subject line
	$mail->Subject = 'Chamado Finalizado';
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	$mail->msgHTML($body);
	//Replace the plain text body with one created manually
	$mail->AltBody = 'This is a plain-text message body';
	//send the message, check for errors
	// $mail->addAddress('kevin.testa@horizonstelecom.com');
	$mail->AddCC('noc@horizonstelecom.com');
	if (!$mail->send()) {
	    echo "Mailer Error: " . $mail->ErrorInfo;	    
	} 

} catch (Exception $e) {

	echo "erro";
}
?>