<?php
namespace Application\Model;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

class Tasks
{   
    private $db, $sql;
    protected $db_name = 'tasks';
    protected $db_user = 'root';
    protected $db_pass = 'root';
    protected $db_host = 'localhost';


    function __construct()
    {   
        $this->db = $this->db_connection();
        $this->sql = new Sql($this->db);
    }

    public function db_connection()
    {
        $db = new DbAdapter(
        array(
            'driver'         => 'Mysqli',
            'database'       => $this->db_name,
            'host'           => $this->db_host,   
            'username'       => $this->db_user,
            'password'       => $this->db_pass,
            )
        );

        return $db;
    }

    public function authentication($user, $password) 
    {
        $select = $this->sql->select();
        $select->from('users');
        $select->where(array(
            'password' => md5($password),
            'login' => $user
        ));
        $selectString = $this->sql->getSqlStringForSqlObject($select);
        $query = $this->db->query($selectString);
        $results = $query->execute();

        return $results;
    }

    public function get_tasks($params) 
    {           
        $select = $this->sql->select();
        $select->from('tasks');
        if (isset($params->id)) {
            $select->where(array('id' => $params->id));
        }
        $selectString = $this->sql->getSqlStringForSqlObject($select);
        $query = $this->db->query($selectString);
        $results = $query->execute();

        return $results;
    }

    public function insert($params) 
    {           
        $insert = $this->sql->insert('tasks');
        $insert->values(array(
            'title' => $params->title, 
            'description' => $params->description, 
            'priority' => $params->priority
        ));

        $insertString = $this->sql->getSqlStringForSqlObject($insert);
        $query = $this->db->query($insertString);
        $results = $query->execute();

        return $results;
    }

    public function update($params, $id) 
    {   
        $update = $this->sql->update('tasks');
        $update->set((array) $params);
        $update->where(array('id' => $id));
        $updateString = $this->sql->getSqlStringForSqlObject($update);
        $query = $this->db->query($updateString);
        $results = $query->execute();

        return $results;
    }

    public function delete($params) 
    {           
        $delete = $this->sql->delete('tasks');
        $delete->where(array('id' => $params->id));
        $deleteString = $this->sql->getSqlStringForSqlObject($delete);
        $query = $this->db->query($deleteString);
        $results = $query->execute();

        return $results;
    }

   
}