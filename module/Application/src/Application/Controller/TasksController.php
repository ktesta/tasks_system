<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class TasksController extends AbstractRestfulController {
	
	public $tasks;

	public function getList()
    {
        return new JsonModel(array('data' => "Welcome to the Zend Framework Tasks API"));
    }

    public function get() 
    {
    	//Requisições no método GET
        $response = $this->getResponse();
        $headers  = $response->getHeaders();

        $method = $this->params()->fromRoute('method'); 
        $data = $this->params()->fromRoute('id'); 
        $content = array(
            "method" => $method,
            "data" => $data,
        );

        $response = $this->getResponse();
        $contentType = 'application/json';
        $adapter = '\Zend\Serializer\Adapter\Json';

        $response->setStatusCode(200);
        $response->getHeaders()->addHeaderLine('Content-Type', $contentType);

        if (method_exists($this, $method)) {
        	$result = $this->$method(json_decode($data));        	
        } else {
        	$result['error']['message'] = "Method $method doesn't exist";
        }

        $adapter = new $adapter;
        $response->setContent($adapter->serialize($result));
        
        return $response;
    }

    public function create() 
    {
    	//Requisições no método POST
        $response = $this->getResponse();
        $headers  = $response->getHeaders();

        $data = json_decode($this->getRequest()->getContent());
        if (isset($data->method)) {
        	$method = $data->method;        	
        } else {
            $return['error']['message'] = 'Method not found';
        	return new JsonModel(array('result' => $result));
        }

        $response = $this->getResponse();
        $adapter = '\Zend\Serializer\Adapter\Json';
        $response->setStatusCode(200);
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        if (method_exists($this, $method)) {
        	$result = $this->$method($data);        	
        } else {
        	$result['error']['message'] = "Method $method doesn't exist";
        }

        $adapter = new $adapter;
        $response->setContent($adapter->serialize($result));
 
        return new JsonModel($result);
    }

    public function getTables($table)
    {
        if (!$this->tasks) {
            $this->tasks = $this->getServiceLocator()->get($table);
            return $this->tasks;
        }
    }

    public function task_create($data)
    {   
        $model = $this->getTables('Application\Model\Tasks');
        $priority_options = array(1, 2, 3);

    	if (!isset($data->title)) {
    		$return['error']['message'] = 'title not found';
    	} else if (!isset($data->description)) {
    		$return['error']['message'] = 'description not found';
    	} else if (!isset($data->priority)) {
    		$return['error']['message'] = 'priority not found';
    	} else if (!in_array($data->priority, $priority_options)) {
    		$return['error']['message'] = "priority can't be $data->priority";
    	} else {
    		$model->insert($data);    		
    		$return['success'] = true;
    	}

        return array('result' => $return);
    }

    public function task_update($data) 
    {
    	$model = $this->getTables('Application\Model\Tasks');
    	$priority_options = array(1, 2, 3);
    	$f = $data->fields;
        $get_task = $model->get_tasks($data);

        $exist = false;
        foreach ($get_task as $get_task) {
            $exist = true;
        }
        if (!$exist) {
            return array('result' => $return['error']['message'] = "ID doesn't exist");
        }

    	if (!isset($data->id)) {
    		$return['error']['message'] = 'id not found';
    	} else if (isset($f->priority) && !in_array($f->priority, $priority_options)) {
    		$return['error']['message'] = "priority can't be $f->priority";
    	} else {
    		$model->update($f, $data->id);    		
    		$return['success'] = true;
    	}

        return array('result' => $return);
    }

    public function task_delete($data) 
    {
    	$model = $this->getTables('Application\Model\Tasks');

    	if (!isset($data->id)) {
    		$return['error']['message'] = 'id not found';
    	} else {
            $get_task = $model->get_tasks($data);
            $exist = false;
            foreach ($get_task as $get_task) {
                $exist = true;
            }
            if (!$exist) {
                return array('result' => $return['error']['message'] = "ID doesn't exist");
            }
            
    		$model->delete($data);    		
    		$return['success'] = true;
    	}

        return array('result' => $return);
    }

    public function task_list($data) 
    {
    	$model = $this->getTables('Application\Model\Tasks');
  		$tasks = $model->get_tasks($data);    		
   		$return['success'] = true;
   		$list = array();
   		foreach ($tasks as $task) {
   			array_push($list, $task);
   		}

   		$return['list'] = $list;
   		$return['success'] = true;

        return array('result' => $return);
    }

    
}
