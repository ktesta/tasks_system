<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\Ldap as AuthAdapter;    
use Zend\Json\Json;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController 
{   
    public $task;

    public function loginAction()
    {  
        $session = new Container();
        $url = 0;

        if( !empty($session->url) ) {
            $url = $session->url ;
        }

        $viewModel = new ViewModel(array('url' => $url));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function listAction()
    {  
        $model = $this->getTables('Application\Model\Tasks');
        $tasks = $model->get_tasks(array());
        $viewModel = new ViewModel(array(
            'tasks' => $tasks
        ));

        return $viewModel;
    }

    public function newTaskAction()
    {  
        $model = $this->getTables('Application\Model\Tasks');
        $viewModel = new ViewModel(array());
        
        return $viewModel;
    }  

    public function createAction()
    {  
        $model = $this->getTables('Application\Model\Tasks');
        $viewModel = new JsonModel(array());
        
        return $viewModel;
    }    

	public function authAction()
    {
        $request = $this->getRequest();
        $postData = $request->getPost()->toArray();
        $password = $postData['password'];

        $model = $this->getTables('Application\Model\Tasks');
        $auth = new AuthenticationService();
        $result = $model->authentication($postData['login'], $password);
        
        $status = 0;
        foreach ($result as $key => $value) {            
            if ($value['login'] == $postData['login']) {                
                $auth->getStorage()->write(array(
                    'login' => $value['login'],
                ));                          
                $status = 1;                
            } else {
                $status = 0;
                $auth->clearIdentity();
            }            
        }

        $jsonModel = new JsonModel(array('status' => $status));

        return $jsonModel;
    }


    public function logoutAction()
    {
        $session = new Container;
        $session->url = null;

        return new JsonModel();
    }

    public function getTables( $table )
    {
        if (!$this->task){
            $this->task = $this->getServiceLocator()->get($table);

            return $this->task;
        }
    }

    
}


