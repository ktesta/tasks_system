<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\Ldap as AuthAdapter;   
use Zend\Session\Container;
use Application\Model\Tasks;
use Zend\View\Model\JsonModel;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        ini_set('display_startup_errors',false);
        ini_set('display_errors',false);
        setlocale(LC_MONETARY, 'pt_BR');
        date_default_timezone_set('America/Sao_Paulo');
        
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $this->checkAuthentication($eventManager);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 0);
        $eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, array($this, 'onRenderError'), 0);
        
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
             'factories' => array(
                 'Application\Model\Tasks' =>  function($sm) {
                     $table = new Tasks;
                     return $table;
                 }
             ),
         );
    }

     public function onDispatchError($e)
    {
        return $this->getJsonModelError($e);
    }

    public function onRenderError($e)
    {
        return $this->getJsonModelError($e);
    }

    public function getJsonModelError($e)
    {
        $error = $e->getError();
        if (!$error) {
            return;
        }

        $response = $e->getResponse();
        $exception = $e->getParam('exception');
        $exceptionJson = array();
        if ($exception) {
            $exceptionJson = array(
                'class' => get_class($exception),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'stacktrace' => $exception->getTraceAsString()
            );
        }

        $errorJson = array(
            'message'   => 'An error occurred during execution; please try again later.',
            'error'     => $error,
            'exception' => $exceptionJson,
        );
        if ($error == 'error-router-no-match') {
            $errorJson['message'] = 'Resource not found.';
        }

        $model = new JsonModel(array('errors' => array($errorJson)));

        $e->setResult($model);

        return $model;
    }

    public function checkAuthentication($eventManager)
    {
        $eventManager->attach(MvcEvent::EVENT_ROUTE, function($e) { 
            
            $routeMatch = $e->getRouteMatch(); 

            $controller = $routeMatch->getParam('controller'); //nome do controller 
            $action = $routeMatch->getParam('action'); //nome da action
            $id = $routeMatch->getParam('id'); //id acessado
            $id = urlencode($id);
            $auth = new AuthenticationService();

            if ($action == 'logout') { //Se a action 'logout' for acionada, limpa a session de autenticação
                $auth->clearIdentity();
            }

            if ($action != 'login' && $action != 'auth' && $controller != 'Application\Controller\Tasks') {
                $user_attr = $auth->getIdentity();
                if (empty($user_attr['login'])) {
                    //Se não estiver logado, redireciona para a tela de login
                    $session = new Container();
                    $session->url = "$action";
                    $response = $e->getResponse();
                    $response->getHeaders()->addHeaderLine('Location', 'login');
                    $response->setStatusCode(302);                
                }
            }
        }); 
    }
}
