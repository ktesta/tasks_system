Olá,

Desenvolvi a aplicação usando Zend Framework. 

Este é o framework para php que eu mais tenho conhecimento.

Para rodar a aplicação, é preciso apenas realizar o git clone.

No entanto, é preciso criar também algumas tabelas no mysql (database.sql).

#Configurações do banco de dados:
```
#!php
    $db_name = 'tasks';
    $db_user = 'root';
    $db_pass = 'root';
    $db_host = 'localhost';
```
Caso necessário alteração, é preciso alterar o arquivo module/Application/src/Application/Model/Tasks.php

Criei uma tela de login para acessar a aplicação. 

As credenciais de acesso são: 

    Login: admin
    Senha: admin

A aplicação usa os métodos da API.
Porém, pelo tempo proposto, não implementei o acesso a API com usuário e senha.

========================================================

#Requisições:

#Listar tarefas
OBS: Caso não informado ID, será retornado todas as tarefas.


```
#!php
curl -X post -d '{"id":"21", "method": "task_list"}' http://localhost/public_html/tasks_system/public/index.php/api/tasks

Get: http://localhost/public_html/api/public/index.php/tasks_system/tasks/task_list/21
```

#Criar tarefas

```
#!php
curl -X post -d '{"title":"titulo", "description":"descrição", "priority":"1", "method": "task_create"}' http://localhost/public_html/tasks_system/public/index.php/api/tasks

Get: http://localhost/public_html/tasks_system/task_create/{"title":"titulo", "description":"descrição", "priority":"1", "method": "task_create"}
```

#Atualizar tarefa
Caso tarefa não exista, mensagem de erro será mostrada.
Só serão atualizados os campos passados.

```
#!php
curl -X post -d '{"fields": {"title":"titulo", "description":"descrição", "priority":"1"}, "id": "28", "method": "task_update"}' http://localhost/public_html/tasks_system/public/index.php/api/tasks

Get: http://localhost/public_html/tasks_system/public/index.php/api/tasks/task_update/{"fields": {"title":"titulo", "description":"descrição", "priority":"1"}, "id": "28", "method": "task_update"}
```

#Deletar Terefa
Caso tarefa não exista, mensagem de erro será mostrada.

```
#!php
curl -X post -d '{"id": "21", "method": "task_delete"}' http://localhost/public_html/tasks_system/public/index.php/api/tasks

Get: http://localhost/public_html/tasks_system/public/index.php/api/tasks/task_delete/{"id": "21", "method": "task_delete"}
```