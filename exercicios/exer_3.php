<?php
namespace Application\Model;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

class MyUserClass
{
	private $db, $sql;
    protected $db_name = 'user';
    protected $db_user = 'user';
    protected $db_pass = 'password';
    protected $db_host = 'localhost';


    function __construct()
    {   
        $this->db = $this->db_connection();
        $this->sql = new Sql($this->db);
    }

    public function db_connection()
    {
        $db = new DbAdapter(
        array(
            'driver'         => 'Mysqli',
            'database'       => $this->db_name,
            'host'           => $this->db_host,   
            'username'       => $this->db_user,
            'password'       => $this->db_pass,
            )
        );

        return $db;
    }

    public function getUserList()
    {
    	$select = $this->sql->select();
    	$select->columns(array('name'));
        $select->from('users');
        $select->order('name ASC');
        $selectString = $this->sql->getSqlStringForSqlObject($select);
        
        $query = $this->db->query($selectString);
        $results = $query->execute();

        return $results;
    }
}

