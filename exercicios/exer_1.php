<?php 
for ($number = 1; $number <= 100; $number++) {
	echo "$number: ";
	if($number % 15 == 0) {
    	echo "FizzBuzz";
	} elseif ($number % 5 == 0) {
	    echo "Buzz";
	} elseif ($number % 3 == 0) {
	    echo "Fizz";
	} else {
		echo $number;
	}
	echo "\n";
}

