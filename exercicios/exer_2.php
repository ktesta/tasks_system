<?php 
//A função isset() verifica se a variável está setada e é diferente de NULL.
//Ou seja, se não quisermos que o usuário seja redirecionado colocamos o valor da session/cookie como null (unset())

function checkAuthentication()
{
    if (isset($_SESSION['loggedin']) || isset($_COOKIE['loggedin'])) {            
       header("Location: http://www.google.com");
    }
}

checkAuthentication();

